﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ZAD4.Structure;

namespace ZAD4 {
    class ViewModelMainWindow : ViewModelBase {
        FileManager fileManager;
        public Document OpenedDocument;

        public List<Film> FilmList {
            get {
                if (OpenedDocument == null)
                    return new List<Film>();
                else
                    return OpenedDocument.Base.Films.List;
            }
        }

        public List<Person> PersonList {
            get {
                if (OpenedDocument == null)
                    return new List<Person>();
                else
                    return OpenedDocument.Base.People.List;
            }
        }

        public Film ChosenFilm {
            get { return Film.Chosen; }
            set { Film.Chosen = value; }
        }

        public Person ChosenPerson {
            get { return Person.Chosen; }
            set { Person.Chosen = value; }
        }

        public bool FileOpened { get { return OpenedDocument != null; } }

        public RelayCommand OpenFileClicked { get; set; }
        public RelayCommand SaveFileClicked { get; set; }
        public RelayCommand SaveFileAsClicked { get; set; }

        public RelayCommand AddFilmClicked { get; set; }
        public RelayCommand EditFilmClicked { get; set; }
        public RelayCommand RemoveFilmClicked { get; set; }

        public RelayCommand AddPersonClicked { get; set; }
        public RelayCommand EditPersonClicked { get; set; }
        public RelayCommand RemovePersonClicked { get; set; }


        public ViewModelMainWindow() {
            Console.WriteLine("START");
            fileManager = new FileManager();
            fileManager.MainWindow = this;
            
            OpenFileClicked = new RelayCommand(OpenFileDialog);
            SaveFileClicked = new RelayCommand(SaveFile);
            SaveFileAsClicked = new RelayCommand(SaveFileAs);

            AddFilmClicked = new RelayCommand(AddFilm);
            EditFilmClicked = new RelayCommand(EditFilm);
            RemoveFilmClicked = new RelayCommand(RemoveFilm);

            AddPersonClicked = new RelayCommand(AddPerson);
            EditPersonClicked = new RelayCommand(EditPerson);
            RemovePersonClicked = new RelayCommand(RemovePerson);
        }

        void OpenFileDialog(object o) {
            fileManager.OpenFile();
        }

        void SaveFile(object o) {
            fileManager.SaveFile();
        }

        void SaveFileAs(object o) {
            fileManager.SaveFileAs();
        }

        public void OpenDocument(Document doc) {
            OpenedDocument = doc;
            UpdateFilmList();
            UpdatePersonList();
            RaisePropertyChanged("FileOpened");
        }

        public void AddFilm(object o) {
            OpenedDocument.Base.Films.AddFilm(OpenedDocument.Base.Films.getEmptyUnique());
            UpdateFilmList();
        }

        public void EditFilm(object o) {
            if (ChosenFilm != null) {
                FilmEditor editor = new FilmEditor();
                ViewModelFilmEditor vm = editor.DataContext as ViewModelFilmEditor;
                editor.Closing += FilmsUpdated;
                Console.WriteLine(ChosenFilm.EnglishTitle + " - " + ChosenFilm.ReleaseDateString);
                editor.ShowDialog();
            }
        }

        public void RemoveFilm(object o) {
            OpenedDocument.Base.Films.RemoveFilm(ChosenFilm);
            
            UpdateFilmList();
        }

        public void AddPerson(object o) {
            OpenedDocument.Base.People.AddPerson(OpenedDocument.Base.People.getEmptyUnique());
            UpdatePersonList();
        }

        public void EditPerson(object o) {
            if (ChosenPerson != null) {
                PersonEditor editor = new PersonEditor();
                editor.Closing += PeopleUpdated;
                editor.ShowDialog();
            }
        }

        public void RemovePerson(object o) {
            if (ChosenPerson != null) {
                OpenedDocument.Base.People.RemovePerson(ChosenPerson);
            }
            UpdatePersonList();
        }

        void FilmsUpdated(object sender,
            System.ComponentModel.CancelEventArgs e) {
            UpdateFilmList();
        }

        void PeopleUpdated(object sender,
            System.ComponentModel.CancelEventArgs e) {
            UpdatePersonList();
        }

        public void UpdateFilmList() {
            Document doc = OpenedDocument;
            OpenedDocument = null;
            RaisePropertyChanged("FilmList");
            OpenedDocument = doc;
            RaisePropertyChanged("FilmList");
        }

        public void UpdatePersonList() {
            Document doc = OpenedDocument;
            OpenedDocument = null;
            RaisePropertyChanged("PersonList");
            OpenedDocument = doc;
            RaisePropertyChanged("PersonList");
        }
    }
}
