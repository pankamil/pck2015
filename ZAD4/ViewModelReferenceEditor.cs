﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using ZAD4.Structure;

namespace ZAD4 {
    class ViewModelReferenceEditor : ViewModelBase {
        public PersonReference EditedPerson { get { return PersonReference.Chosen; } }
        public RelayCommand CloseClicked { get; private set; }
        public List<Person> People { get { return Document.Instance.Base.People.List; } }
        private int _index;
        public int ChosenIndex {
            get { return _index; }
            set {
                _index = value;
                EditedPerson.setID(People[_index].ID);
            }
        }

        public ViewModelReferenceEditor() {
            CloseClicked = new RelayCommand(close);
            if (EditedPerson.ID == "NONE") {
                ChosenIndex = 0;
            } else {
                int index = 0;
                foreach(Person p in People) {
                    if (p.ID == EditedPerson.ID) break;
                    index++;
                }
                ChosenIndex = index;
            }
            RaisePropertyChanged("ChosenIndex");
        }

        void close(object o) {
            try {
                ((Window)o).Close();
            }catch (Exception) {

            }
        }
    }
}
