﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ZAD4.Structure;

namespace ZAD4 {
    class ViewModelFilmEditor : ViewModelBase {

        public Film Film { get { return Film.Chosen; } }

        bool readLock = false;

        public List<Words> Titles {
            get {
                if (!readLock)
                    return Film.Title.Versions;
                else return null;
            }
        }
        public Words ChosenTitle { get; set; }

        public string ID { get { return Film.ID; } }
        public string ReleaseDateString { get { return Film.ReleaseDateString; } }

        public RelayCommand AddTitleClicked { get; private set; }
        public RelayCommand RemoveTitleClicked { get; private set; }

        public RelayCommand AddDirectorClicked { get; private set; }
        public RelayCommand RemoveDirectorClicked { get; private set; }

        private DateTime _releasePick;
        public DateTime ReleasePickValue {
            get { return _releasePick; }
            set {
                _releasePick = value;
                setDate(value);
            }
        }

        public List<Director> Directors { get { if (!readLock) return Film.Direction.List; else return null; } }
        public Director ChosenDirector { get; set; }

        public string Description {
            get { return Film.Description; }
            set { Film.Description = value; }
        }

        public ViewModelFilmEditor() {
            AddTitleClicked = new RelayCommand(AddTitle);
            RemoveTitleClicked = new RelayCommand(RemoveTitle);
            Date.Chosen = Film.Chosen.ReleaseDate;

            ReleasePickValue = Date.Chosen.Get();
            RaisePropertyChanged("ReleasePickValue");

            AddDirectorClicked = new RelayCommand(addDirector);
            RemoveDirectorClicked = new RelayCommand(removeDirector);
        }

        void AddTitle(object o) {
            Film.Title.addVersion(Words.getEmpty());
            updateTitleList();
        }

        void RemoveTitle(object o) {
            if (ChosenTitle != null) {
                Film.Title.removeVersion(ChosenTitle);
                updateTitleList();
            }
        }

        void setDate(DateTime dt) {
            Film.ReleaseDate.Set(dt);
            RaisePropertyChanged("ReleaseDateString");
        }


        void updateTitleList() {
            readLock = true;
            RaisePropertyChanged("Titles");
            readLock = false;
            RaisePropertyChanged("Titles");
        }

        void addDirector(object o) {
            Director neu = Director.getEmpty();
            Film.Direction.add(neu);
            PersonReference.Chosen = neu;

            DirectorEditor editor = new DirectorEditor();
            editor.Closing += DirectorsUpdated;
            editor.ShowDialog();
            
        }

        void removeDirector(object o) {
            Film.Direction.remove(ChosenDirector);
            readLock = true;
            RaisePropertyChanged("Directors");
            readLock = false;
            RaisePropertyChanged("Directors");
        }

        void DirectorsUpdated(object sender,
            System.ComponentModel.CancelEventArgs e){
            readLock = true;
            RaisePropertyChanged("Directors");
            readLock = false;
            RaisePropertyChanged("Directors");
        }
    }
}
