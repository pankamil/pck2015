﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ZAD4.Extensions {
    public static class XmlExtensions {
        public static XmlNode GetChildNodeByName(this XmlNode node, string name) {
            foreach (XmlNode n in node.ChildNodes) {
                if (n.Name == name) {
                    return n;
                }
            }
            return null;
        }

        public static XmlNode GetNodeByName(this XmlNodeList list, string name) {
            foreach (XmlNode n in list) {
                if (n.Name == name) {
                    return n;
                }
            }
            return null;
        }

        
    }
}
