﻿using Microsoft.Win32;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Schema;

namespace ZAD4 {
    class FileManager {
        public ViewModelMainWindow MainWindow { get; set; }

        private OpenFileDialog openFileDialog;
        private SaveFileDialog saveFileDialog;

        public FileInfo OpenedFileInfo { get; private set; }

        public void OpenFile() {
            Console.WriteLine("OPEN");
            openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "XML Files (*.xml)|*.xml";
            openFileDialog.FilterIndex = 0;
            openFileDialog.DefaultExt = "xml";
            openFileDialog.Multiselect = false;
            openFileDialog.FileOk += FileOpened;
            openFileDialog.ShowDialog();
        }

        public void SaveFile() {
            SaveFileToPath( OpenedFileInfo.FullName);
            OpenFileFromPath(OpenedFileInfo.FullName);
        }

        public void SaveFileAs() {
            saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "XML Files (*.xml)|*.xml";
            saveFileDialog.FilterIndex = 0;
            saveFileDialog.DefaultExt = "xml";
            saveFileDialog.FileOk += FileSaved;
            saveFileDialog.ShowDialog();
        }

        void FileOpened(object sender,
            System.ComponentModel.CancelEventArgs e) {
            OpenFileFromPath( openFileDialog.FileName);
        }

        void OpenFileFromPath(string fileName) {
            Console.WriteLine(fileName);
            bool validationSuccess = validate(fileName);
            if (validationSuccess)
                System.Windows.MessageBox.Show("Walidacja zakończona powodzeniem!");
                

            try {
                OpenedFileInfo = new FileInfo(fileName);
                FileStream fileStream = OpenedFileInfo.OpenRead();

                string fileContents;
                using (StreamReader reader = new StreamReader(fileStream)) {
                    fileContents = reader.ReadToEnd();
                }

                Document doc = new Document(fileContents);

                if (!validationSuccess)
                    System.Windows.MessageBox.Show("Walidacja zakończona niepowodzeniem, jednak wczytywanie się powiodło!");
                MainWindow.OpenDocument(doc);
            } catch (Exception e) {
                System.Windows.MessageBox.Show("Błąd wczytywania!");
            }
        }

        void SaveFileToPath(string fileName) {
            Document.Instance.Xml.Save(fileName);
        }

        void FileSaved(object sender,
            System.ComponentModel.CancelEventArgs e) {
            string path = saveFileDialog.FileName;
            SaveFileToPath(path);
            OpenFileFromPath(path);
        }

        bool isValid = true;
        bool validate(string fileName) {
            isValid = true;
            XmlReaderSettings s = new XmlReaderSettings();
            s.XmlResolver = new XmlUrlResolver();
            s.DtdProcessing = DtdProcessing.Parse;
            s.ValidationType = ValidationType.DTD;
            s.IgnoreWhitespace = true;
            s.ValidationEventHandler += new ValidationEventHandler(ValidationHandler);
            XmlReader r = XmlReader.Create(fileName, s);
            while (r.Read() && isValid) ;
            return isValid;
            /*XmlTextReader tr = new XmlTextReader(fileName);
            XmlValidatingReader vr = new XmlValidatingReader(tr);

            vr.ValidationType = ValidationType.DTD;
            vr.ValidationEventHandler 

            while (vr.Read()) ;
            Console.WriteLine("Validation finished");
            return isValid;*/
        }

        void ValidationHandler(object sender,
            ValidationEventArgs args) {
            isValid = false;
            Console.WriteLine("Validation event\n" + args.Message);
        }
    }
}
