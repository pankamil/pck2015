﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ZAD4.Structure {
    
    class Base {
        public XmlNode node;

        public Header Header;
        public People People;
        public Films Films;

        public Base(Document doc) {
            this.node = doc.Xml.GetElementsByTagName("base")[0];
            Console.WriteLine(node.Name);

            Header = new Header(doc.Xml.GetElementsByTagName("header")[0]);
            People = new People(doc.Xml.GetElementsByTagName("people")[0]);
            Films = new Films(doc.Xml.GetElementsByTagName("films")[0]);
        }
    }
}
