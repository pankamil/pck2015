﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ZAD4.Structure {
    class Title {
        public XmlNode node;

        public List<Words> Versions = new List<Words>();

        public Title(XmlNode node) {
            this.node = node;
            Console.WriteLine(node.Name);
            XmlNodeList list = node.ChildNodes;
            foreach(XmlNode n in list) {
                Words w = new Words(n);
                Versions.Add(w);
            }
        }

        public void addVersion(Words w) {
            Versions.Add(w);
            node.AppendChild(w.node);
        }

        public void removeVersion(Words w) {
            Versions.Remove(w);
            node.RemoveChild(w.node);
        }

        public Words this[string lang] {
            get {
                foreach (Words w in Versions)
                    if (w.Language == lang)
                        return w;
                return Versions[0];
            }
        }
    }
}
