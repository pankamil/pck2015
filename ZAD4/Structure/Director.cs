﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ZAD4.Structure {
    class Director :PersonReference {
        public Director(XmlNode node) : base(node){

        }

        private static string emptyNode = "<director id='NONE'>None</director>";
        public static Director getEmpty() {
            XmlNode nod = Document.Instance.ImportNode(emptyNode);
            return new Director(nod);
        }
    }
}
