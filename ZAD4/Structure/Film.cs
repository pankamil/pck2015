﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using ZAD4.Extensions;

namespace ZAD4.Structure {
    class Film {
        public XmlNode node;
        public static Film Chosen { get; set; }
        public string ID {
            get { return node.Attributes["id"].InnerText; }
            set { node.Attributes["id"].InnerText = value; }
        }

        public Title Title { get; private set; }
        public string EnglishTitle { get { return Convert.ToString(Title["pl"]); } }

        public Date ReleaseDate { get; private set; }
        public string ReleaseDateString { get { return ReleaseDate.ToString(); } }

        public string Description {
            get { return node.GetChildNodeByName("description").InnerText; }
            set { node.GetChildNodeByName("description").InnerText = value; }
        }
        
        public Direction Direction { get; private set; }

        public Film(XmlNode nod, bool needsImporting = false) {
            if (needsImporting) {
                XmlNode n = Document.Instance.Xml.ImportNode(nod, true);
                this.node = n;
            } else
                this.node = nod;
            Console.WriteLine(node.Name + " " + ID);
            XmlNodeList list = node.ChildNodes;

            Title = new Title(list.GetNodeByName("title"));
            ReleaseDate = new Date(list.GetNodeByName("date"));
            Direction = new Direction(list.GetNodeByName("direction"));
            Console.WriteLine("Description: " + Description);
        }

        public static Film getEmpty() {
            XmlNode nod =  Document.Instance.ImportNode(emptyNode);
            return new Film(nod);
        }

        private static string emptyNode = @"<film id='ID'>
			<title>
				<words lang='en'>Title</words>
			</title>
			<date day='1' month='1' year='2001'/>
			<time unit='min'>120</time>
			<genres>
				<genre>comedy</genre>
			</genres>
			<direction>

			</direction>
			<screenplay>
			
			</screenplay>
			<cast>

			</cast>
			<budget unit='mln USD'>60</budget>
			<boxoffice unit='mln USD'>60</boxoffice>
                
			<reception>
			</reception>
			<description>Description</description>
		</film>";
    }
}
