﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ZAD4.Structure {
    class Direction {
        public XmlNode node;
        public List<Director> List = new List<Director>();

        public Direction(XmlNode node) {
            
            this.node = node;
            Console.WriteLine(node.Name);
            XmlNodeList list = node.ChildNodes;

            foreach (XmlNode n in list) {
                List.Add(new Director(n));
            }
        }

        public void add(Director d) {
            node.AppendChild(d.node);
            List.Add(d);
        }

        public void remove(Director d) {
            node.RemoveChild(d.node);
            List.Remove(d);
        }
    }
}
