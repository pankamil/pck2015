﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using ZAD4.Extensions;

namespace ZAD4.Structure {
    class PersonReference {
        public static PersonReference Chosen { get; set; }

        public XmlNode node;
        public string ID {
            get { return node.Attributes["id"].InnerText; }
            private set { node.Attributes["id"].InnerText = value; }
        }

        public string Name {
            get { return node.InnerText; }
            set { node.InnerText = value; }
        }

        public bool setID(string ID) {
            Person p = Document.Instance.Base.People.GetPersonByID(ID);
            if (p == null)
                return false;
            else {
                this.ID = p.ID;
                Name = p.Name;
                return true;
            }
        }

        public PersonReference(XmlNode node) {
            this.node = node;
            Console.WriteLine(node.Name + " " + Name+ " (" + ID + ")");
        }
    }
}
