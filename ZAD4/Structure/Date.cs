﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ZAD4.Structure {
    public class Date {
        public XmlNode node;
        public static Date Chosen;
        public int Day {
            get { return Int32.Parse(node.Attributes["day"].InnerText); }
            set { node.Attributes["day"].InnerText = value.ToString(); }
        }

        public int Month {
            get { return Int32.Parse(node.Attributes["month"].InnerText); }
            set { node.Attributes["month"].InnerText = value.ToString(); }
        }

        public int Year {
            get { return Int32.Parse(node.Attributes["year"].InnerText); }
            set { node.Attributes["year"].InnerText = value.ToString(); }
        }

        public Date(XmlNode node) {
            this.node = node;
            Console.WriteLine(ToString());
        }

        public void Set(DateTime dt) {
            Console.WriteLine(dt.ToString());
            Day = dt.Day;
            Month = dt.Month;
            Year = dt.Year;
        }

        public DateTime Get() {
            DateTime result;
            try {
                result = new DateTime(Year, Month, Day);
            } catch (Exception) {
                result = new DateTime(Year, Month, 1);
            }

            return result;
        }

        public override string ToString() {
            return Day + "/" + Month + "/" + Year;
        }
    }
}
