﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ZAD4.Structure {
    class Words {
        public XmlNode node;

        public string Language {
            get { return node.Attributes["lang"].InnerText; }
            set { node.Attributes["lang"].InnerText = value; }
        }
        public string Text {
            get { return node.InnerText; }
            set { node.InnerText = value; }
        }

        public Words(XmlNode node) {
            this.node = node;
            Console.WriteLine(node.Name + " " + Language + " " + Text);
        }

        public override string ToString() {
            return Text + " (" + Language + ")";
        }

        private static string emptyNode = "<words lang='en'>Words</words>";

        public static Words getEmpty() {
            XmlNode nod = Document.Instance.ImportNode(emptyNode);
            return new Words(nod);
        }
    }
}
