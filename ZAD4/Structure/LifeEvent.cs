﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using ZAD4.Extensions;

namespace ZAD4.Structure {
    class LifeEvent {
        public XmlNode node;
        public Date Date;

        public string Place {
            get { return node.GetChildNodeByName("place").InnerText; }
            set { node.GetChildNodeByName("place").InnerText = value; }
        }

        public LifeEvent(XmlNode node) {
            this.node = node;
            Date = new Date(node.GetChildNodeByName("date"));
            if (node.GetChildNodeByName("place") == null) {
                XmlNode place = Document.Instance.Xml.CreateAttribute("place");
                node.AppendChild(place);
            }

            Console.WriteLine(node.Name + ": " + Date.ToString() + ", " + Place);
        }

        public static LifeEvent getEmptyDeath() {
            XmlNode nod = Document.Instance.ImportNode(emptyDeath);
            return new LifeEvent(nod);
        }

        private static string emptyDeath =
        @"<death>
                <date day='1' month='1' year='2001' />
                <place>Place</place>
            </death>";
    }
}
