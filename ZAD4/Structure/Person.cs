﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using ZAD4.Extensions;

namespace ZAD4.Structure {
    class Person {
        public XmlNode node;
        public static Person Chosen;

        public string ID {
            get { return node.Attributes["id"].InnerText; }
            set { node.Attributes["id"].InnerText = value; }
        }
        public string Name {
            get { return node.GetChildNodeByName("name").InnerText; }
            set { node.GetChildNodeByName("name").InnerText = value; }
        }

        public LifeEvent Birth { get; private set; }
        public LifeEvent Death { get; private set; }

        public string BirthDateString { get { return Birth.Date.ToString(); } }

        public Person (XmlNode node) {
            this.node = node;
            Console.WriteLine(node.Name + " " + Name);
            Birth = new LifeEvent(node.GetChildNodeByName("birth"));
            XmlNode d = node.GetChildNodeByName("death");
            if (d != null)
                Death = new LifeEvent(d);


        }

        public void addDeath() {
            if (Death == null) {
                Death = LifeEvent.getEmptyDeath();
                node.AppendChild(Death.node);
            }
        }

        public void removeDeath() {
            if (Death != null) {
                node.RemoveChild(Death.node);
                Death = null;
            }
        }

        public override string ToString() {
            return Name + " (" + Birth.Date.ToString() + ")";
        }

        public static Person getEmpty() {
            XmlNode nod = Document.Instance.ImportNode(emptyNode);
            return new Person(nod);
        }

        private static string emptyNode =
        @"<person id = 'NONE' >
          <name>Name</name>
          <birth>
            <date day = '1' month='1' year='2001' />
            <place>Mordor</place>
          </birth>
          <death>
            <date day = '2' month= '1' year= '2001' />
            <place>Mordor</place>
          </death>
        </person>";
    }
}
