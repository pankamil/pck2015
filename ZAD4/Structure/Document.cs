﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Xml;
using System.Text;

using ZAD4.Structure;

namespace ZAD4 {
    class Document {
        public XmlDocument Xml { get; private set; }
        public Base Base { get; private set; }

        public static Document Instance { get; private set; }

        public Document(string text) {
            
            Film.Chosen = null;
            Date.Chosen = null;
            Person.Chosen = null;

            Xml = new XmlDocument();
            Xml.LoadXml(text);

            Base = new Base(this);
            Instance = this;
        }

        public XmlNode ImportNode(string node) {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(node);
            XmlNode nod = Xml.ImportNode(doc.DocumentElement, true);
            return nod;
        }
    }
}
