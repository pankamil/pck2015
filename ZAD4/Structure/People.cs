﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using ZAD4.Extensions;

namespace ZAD4.Structure {
    class People {
        XmlNode node;
        public List<Person> List = new List<Person>();

        public People(XmlNode node) {
            this.node = node;
            Console.WriteLine(node.Name);

            XmlNodeList list = node.ChildNodes;

            foreach (XmlNode n in list) {
                List.Add(new Person(n));
            }
        }

        public Person GetPersonByID(string ID) {
            foreach (Person p in List) {
                if (p.ID == ID)
                    return p;
            }
            return null;
        }

        public bool AddPerson(Person f) {

            List.Add(f);
            node.AppendChild(f.node);
            return true;
        }

        public bool RemovePerson(Person f) {
            if (List.Remove(f)) {
                node.RemoveChild(f.node);
                return true;
            }else
                return false;
        }

        public Person getEmptyUnique() {
            Person p = Person.getEmpty();
            bool unique = true;
            do {
                p.ID = Superstring.RandomString(6);
                foreach (Person pe in List) {
                    if (pe.ID == p.ID) {
                        unique = false;
                        break;
                    }
                }
            } while (!unique);

            node.AppendChild(p.node);
            return p;
        }
    }
}
