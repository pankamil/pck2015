﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

using ZAD4.Extensions;

namespace ZAD4.Structure {
    class Films {
        public XmlNode node;
        public List<Film> List = new List<Film>();

        public Films(XmlNode node) {
            this.node = node;
            Console.WriteLine(node.Name);
            XmlNodeList list = node.ChildNodes;

            foreach(XmlNode n in list) {
                List.Add(new Film(n));
            }
        }

        public bool AddFilm(Film f) {
            
            List.Add(f);
            node.AppendChild(f.node);
            return true;
        }

        public bool RemoveFilm(Film f) {
            List.Remove(f);
            if (Film.Chosen != null) {
                node.RemoveChild(f.node);
                return true;
            } else
                return false;
        }

        public Film getEmptyUnique() {
            Film f = Film.getEmpty();
            bool unique = true;
            do {
                f.ID = Superstring.RandomString(6);
                foreach (Film fi in List) {
                    if (fi.ID == f.ID) {
                        unique = false;
                        break;
                    }
                }
            } while (!unique);
            
            node.AppendChild(f.node);
            return f;
        }
    }
}
