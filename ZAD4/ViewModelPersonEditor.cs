﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZAD4.Structure;
using System.Windows;

namespace ZAD4 {
    class ViewModelPersonEditor : ViewModelBase {
        public Person Person { get { return Person.Chosen; } }
        public string ID { get { return Person.ID; } }
        public string Name {
            get { return Person.Name; }
            set { Person.Name = value; }
        }

        public DateTime BirthDate {
            get { return Person.Birth.Date.Get(); }
            set {
                Person.Birth.Date.Set(value);
            }
        }

        public bool Died { get { return Person.Death != null; } }
        public DateTime DeathDate {
            get { if (Person.Death == null) return BirthDate; else return Person.Death.Date.Get(); }
            set {
                if (Person.Death != null) Person.Death.Date.Set(value);
            }
        }

        public RelayCommand CloseClicked { get; private set; }
        public RelayCommand AddDeathClicked { get; private set; }
        public RelayCommand RemoveDeathClicked { get; private set; }

        public ViewModelPersonEditor() {
            CloseClicked = new RelayCommand(close);
            AddDeathClicked = new RelayCommand(die);
            RemoveDeathClicked = new RelayCommand(live);
        }

        void close(object o) {
            ((Window)o).Close();
        }

        void die(object o) {
            Person.addDeath();
            RaisePropertyChanged("Died");
            RaisePropertyChanged("DeathDate");
        }

        void live(object o) {
            Person.removeDeath();
            RaisePropertyChanged("Died");
            RaisePropertyChanged("DeathDate");
        }
    }
}
