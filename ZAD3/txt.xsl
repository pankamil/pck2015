<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="text"/>
	
	<xsl:template match="header">
		<xsl:text>Autorzy: </xsl:text>
		<xsl:for-each select="student">
		    <xsl:value-of select="."/>
		    <xsl:if test="position() != last()">
		    	<xsl:text>, </xsl:text>
		    </xsl:if>
	    </xsl:for-each>
	</xsl:template>
	
	<xsl:template match="films">
		<xsl:text>&#xa;Informacje o </xsl:text>
 		<xsl:value-of select="./count"/> <xsl:text> filmach.&#xa;</xsl:text>
 		
 		<!--pomocnicza zmienna dla tokenize-->
 		<xsl:variable name="result">
			<xsl:apply-templates select="film"/>
		</xsl:variable>
				
		<xsl:for-each select="tokenize($result, '&#xa;')">
			<xsl:variable name="line">
    		<xsl:call-template name="padding">
				<xsl:with-param name="string" select="."/>
				<xsl:with-param name="pad-char" select="' '"/>
				<xsl:with-param name="str-length" select="80"/>
			</xsl:call-template>
			</xsl:variable>
			<xsl:choose>
				<xsl:when test="contains(., '═')">
					<xsl:value-of select="concat('&#xa;', $line)"/>
				</xsl:when>
				<xsl:when test="contains(., '&#160;')">
					<xsl:text>&#xa;</xsl:text>
				</xsl:when>
				<xsl:when test="contains(., '─')">
					<xsl:value-of select="concat('&#xa;', $line)"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="concat('&#xa;║', $line, '║')"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</xsl:template>
	
	<xsl:template name="padding">
		<xsl:param name="string"/>
		<xsl:param name="pad-char"/>
		<xsl:param name="str-length"/>
		<xsl:choose>
			<xsl:when test="string-length($string) > $str-length and not(contains($string,'═')) and not(contains($string,'─'))">
				<xsl:value-of select="substring($string,1,$str-length)" />   
			</xsl:when>
			<xsl:when test="string-length($string) = $str-length or contains($string,'═') or contains($string,'─')">
				<xsl:value-of select="$string" />   
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="padding">
					<xsl:with-param name="string" select="concat($string,$pad-char)" />
					<xsl:with-param name="pad-char" select="$pad-char" />
					<xsl:with-param name="str-length" select="$str-length" />
				</xsl:call-template>
			</xsl:otherwise>     
		</xsl:choose>   
	</xsl:template>

	<xsl:template match="title">
		<xsl:text>╔════════════════════════════════════════════════════════════════════════════════╗</xsl:text>
		<xsl:text>&#xa;Tytuł: </xsl:text>
		<xsl:for-each select="words">
			<xsl:choose>
		    <xsl:when test="@lang != 'en'">
		    	<xsl:value-of select="concat('&#xa;  (' , . , ')')"/>
		    </xsl:when>
		    <xsl:otherwise>
		    	<xsl:value-of select="concat('&#xa;  ', .)"/>
		    </xsl:otherwise>
		    </xsl:choose>
	    </xsl:for-each>
	    <xsl:text>&#xa;╟────────────────────────────────────────────────────────────────────────────────╢</xsl:text>
	</xsl:template>
	
	<xsl:template match="date">
		<xsl:value-of select="concat('&#xa;Data wydania: ',@day,'.',@month,'.',@year)"/>
	</xsl:template>
	
	<xsl:template match="age">
		<xsl:value-of select="concat('(' , . , ' lat temu)')"/>
	</xsl:template>
	
	<xsl:template match="time">
		<xsl:value-of select="concat('&#xa;Czas trwania: ', . , ' ' , @unit)"/>
	</xsl:template>
	
	<xsl:template match="genres">
		<xsl:text>&#xa;╟────────────────────────────────────────────────────────────────────────────────╢</xsl:text>
		<xsl:text>&#xa;Gatunki: </xsl:text>
		<xsl:for-each select="genre">
			<xsl:text>&#xa;  </xsl:text>
		    <xsl:value-of select="."/>
	    </xsl:for-each>
	</xsl:template>
	
	<xsl:template match="direction">
		<xsl:text>&#xa;╟────────────────────────────────────────────────────────────────────────────────╢</xsl:text>
		<xsl:text>&#xa;Reżyseria: </xsl:text>
		    <xsl:apply-templates select="./director"/>
	</xsl:template>
	
	<xsl:template match="screenplay">
		<xsl:text>&#xa;╟────────────────────────────────────────────────────────────────────────────────╢</xsl:text>
	
		<xsl:text>&#xa;Twórcy scenariusza:</xsl:text>
		
		<xsl:text>&#xa;Scenarzyści:</xsl:text>
		<xsl:apply-templates select="./writer"/>
		
		<xsl:if test="story">
	    	<xsl:text>&#xa;Twórcy historii:</xsl:text>
		    <xsl:apply-templates select="./story"/>
	    </xsl:if>
	    
	    <xsl:if test="basedon">
	    	<xsl:text>&#xa;Na podstawie </xsl:text>
		    <xsl:apply-templates select="./basedon"/>
	    </xsl:if>
	</xsl:template>
	
	<xsl:template match="basedon">
		<xsl:choose>
			<xsl:when test="@work != 'various'">
		    	<xsl:value-of select="concat('&quot;', @work, '&quot;', ' autorstwa:')"/>
		    </xsl:when>
		    <xsl:otherwise>
		    	<xsl:text>dzieł różnych autorstwa:</xsl:text>
		    </xsl:otherwise>
		</xsl:choose>
		<xsl:apply-templates/>
	</xsl:template>
	
	<xsl:template match="cast">
		<xsl:text>&#xa;╟────────────────────────────────────────────────────────────────────────────────╢</xsl:text>
		<xsl:text>&#xa;Obsada: </xsl:text>
		    <xsl:apply-templates select="./actor"/>
	</xsl:template>
	
	<xsl:template match="actor">
		<xsl:apply-templates/>
		<xsl:value-of select="concat('&#xa;    jako: ', @character)"/>
	</xsl:template>
	
	<xsl:template match="budget">
		<xsl:text>&#xa;╟────────────────────────────────────────────────────────────────────────────────╢</xsl:text>

		<xsl:value-of select="concat('&#xa;Budżet:    ', . , ' ', @unit)"/>
	</xsl:template>
	
	<xsl:template match="boxoffice">
		<xsl:value-of select="concat('&#xa;Przychód:  ', . , ' ', @unit)"/>
	</xsl:template>
	
	<xsl:template match="profit">
		<xsl:value-of select="concat('&#xa;Zysk:      ', . , ' ', @unit)"/>
	</xsl:template>
	
	<xsl:template match="reception">
		<xsl:text>&#xa;╟────────────────────────────────────────────────────────────────────────────────╢</xsl:text>
		<xsl:text>&#xa;Oceny:</xsl:text>
		<xsl:apply-templates/>
	</xsl:template>
	
	<xsl:template match="zeroten">
		<xsl:value-of select="concat('&#xa;  ', @source, ':             ', .)"/>
	</xsl:template>
	
	<xsl:template match="percent">
		<xsl:value-of select="concat('&#xa;  ', @source, ':  ', .)"/>
	</xsl:template>
	
	<xsl:template match="description">
		<xsl:text>&#xa;╟────────────────────────────────────────────────────────────────────────────────╢</xsl:text>
		<xsl:text>&#xa;Opis:&#xa;</xsl:text>
		<xsl:value-of select="replace(concat(normalize-space(.),' '), '(.{0,80}) ', '$1&#xa;')"/>
		<xsl:text>╚════════════════════════════════════════════════════════════════════════════════╝&#xa;&#160;&#xa;</xsl:text>
	</xsl:template>
	
	<xsl:template match="person">
		<xsl:value-of select="concat('&#xa;  ', name)"/>
		<xsl:value-of select="concat('&#xa;    Data urodzenia: ',birth/date/@day,'.',birth/date/@month,'.',birth/date/@year, ' (', ageDuringPremiere, ' lat podczas premiery)')"/>
		<xsl:if test="death">
			<xsl:value-of select="concat('&#xa;    Data śmierci: ',death/date/@day,'.',death/date/@month,'.',death/date/@year)"/>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
