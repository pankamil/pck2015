<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" encoding="utf-8"/>
  	<xsl:template match="/base">
  	<xsl:text disable-output-escaping="yes">&lt;!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"&gt;&#xa;
</xsl:text>
  	<xsl:text disable-output-escaping="yes">&lt;html xmlns="http://www.w3.org/1999/xhtml"&gt;&#xa;</xsl:text>
  			<head>
				<meta http-equiv="content-type" content="application/xhtml+xml; charset=utf-8" />
				<title>FILMY</title>
			</head>
			<body>
  			<xsl:attribute name="xmlns">http://www.w3.org/1999/xhtml</xsl:attribute>
  			<xsl:text>&#xa;</xsl:text>
  			
  			<xsl:element name="p">
  			<xsl:attribute name="style">font-size:700%; margin-top: 0.2em; margin-bottom: 0em;</xsl:attribute>
  			FILMY
  			</xsl:element>
			<xsl:apply-templates select="films|header"/>
			<xsl:apply-templates select="acclaim"/>
			<xsl:apply-templates select="cashy"/>
			</body>
			<xsl:text disable-output-escaping="yes"> &lt;/html&gt;</xsl:text>
	</xsl:template>
	<xsl:template match="base/header">
		<xsl:element name="p">
		<xsl:attribute name="style">font-size:300%; margin-top: 0em; margin-bottom: 0.5em;</xsl:attribute>
		<xsl:text>Autorzy: </xsl:text>
		<xsl:for-each select="student">
		    <i>
		    	<xsl:value-of select="."/>
		    </i
>
		    <xsl:if test="position() != last()">
		    <xsl:text>, </xsl:text>
		    </xsl:if>
	    </xsl:for-each>
	    </xsl:element>
	</xsl:template>
	<xsl:template match="base/films">
		<xsl:element name="p">
		<xsl:attribute name="style">font-size:200%; margin-top: 1em; margin-bottom: 0.5em;</xsl:attribute>
		<xsl:text>Informacje o </xsl:text>
 <xsl:value-of select="./count"/> <xsl:text> filmach.</xsl:text>
	    </xsl:element>
	    <xsl:apply-templates select="film"/>
	</xsl:template>
	<xsl:template match="films/film">
	<div style="margin-bottom:4em">
	
	
<xsl:element name="table">
	
<xsl:attribute name="style">width:100%</xsl:attribute>
	
<xsl:attribute name="id"><xsl:value-of select="concat(./title/words[@lang='en'], ./date/@year)"/></xsl:attribute>
		<tr>
		    <td width="12%" valign="top">Tytuł</td>
		    <td>
		    <xsl:for-each select="./title/words">
		    <xsl:if test="position() = 1">
		    <span style="font-size:150%"><xsl:value-of select="."/></span>
		    </xsl:if>
		    <xsl:if test="position() != 1">
		    <xsl:value-of select="."/>
		    </xsl:if>
		    <xsl:text> (</xsl:text><xsl:value-of select="./@lang"/> <xsl:text>)</xsl:text>
		    <br />
		    </xsl:for-each></td> 
	    </tr>
	    <tr>
		    <td valign="top">Data premiery</td>
		    <td>
		    <xsl:apply-templates select="./date"/>
		    <xsl:text> </xsl:text>
		    <xsl:apply-templates select="./age"/>
			</td> 
	    </tr>
	    <tr>
		    <td valign="top">Reżyseria</td>
		    <td>
		    <xsl:apply-templates select="./direction"/>
			</td> 
	    </tr>
	    <tr>
		    <td valign="top">Twórcy scenariusza</td>
		    <td>
		    <xsl:apply-templates select="./screenplay"/>
			</td> 
	    </tr>
	    <tr>
		    <td valign="top">Obsada</td>
		    <td>
		    <xsl:apply-templates select="./cast"/>
			</td> 
	    </tr>
	    <tr>
		    <td valign="top">Opis</td>
		    <td>
		    <xsl:value-of select="./description"/>
			</td> 
	    </tr>
	    <tr>
		    <td valign="top">Finanse</td>
		    <td>
		    <xsl:apply-templates select="./budget | ./boxoffice | ./profit"/>
			</td> 
	    </tr>
	    <tr>
		    <td valign="top">Oceny</td>
		    <td>
		    <xsl:apply-templates select="./reception"/>
			</td> 
	    </tr>
	    
    </xsl:element>
    </div>
	</xsl:template>
	
	<xsl:template match="date">
	<xsl:value-of select="./@day"/>
	<xsl:text>.</xsl:text>
	<xsl:value-of select="./@month"/>
	<xsl:text>.</xsl:text>
	<xsl:value-of select="./@year"/>
	</xsl:template>
	
	<xsl:template match="age">
	<xsl:if test=". = 0">
	<xsl:text>(w tym roku)</xsl:text>
	</xsl:if>
	<xsl:if test=". > 0">
	<xsl:text>(</xsl:text><xsl:value-of select="."/>
	<xsl:if test=". = 1">
    <xsl:text> rok temu)</xsl:text>
    </xsl:if>
    <xsl:if test=". &gt; 1 and . &lt; 5">
    <xsl:text> lata temu)</xsl:text>
    </xsl:if>
    <xsl:if test=". &gt; 5 or . = 5 ">
    <xsl:text> lat temu)</xsl:text>
    </xsl:if>
    </xsl:if>
	</xsl:template>
	
	
	<xsl:template match="person">
	<xsl:value-of select="./name"/>
	<ul style="margin-top:0em; margin-bottom:0em" >
	<li>Data urodzenia: <xsl:apply-templates select="./birth/date"/> <xsl:text> (</xsl:text>  <xsl:value-of select="./ageDuringPremiere"/> <xsl:text> lat podczas premiery)</xsl:text></li>
	<xsl:if test="boolean(./death/date)">
	<li>Data śmierci: <xsl:apply-templates select="./death/date"/></li>
	</xsl:if>
	</ul>
	</xsl:template>
	
	
	<xsl:template match="film/direction">
	<xsl:apply-templates select="./director"/>
	</xsl:template>
	
	<xsl:template match="direction/director">
	<xsl:apply-templates/>
	</xsl:template>
	
	
	<xsl:template match="film/screenplay">
	<xsl:if test="boolean(./writer)">
	<span><b>Scenarzyści:</b></span><br />
	<xsl:apply-templates select="./writer"/>
	<br />
	</xsl:if>
	<xsl:if test="boolean(./story)">
	<span><b>Twórcy historii:</b></span><br />
	<xsl:apply-templates select="./story"/>
	<br />
	</xsl:if>
	<xsl:if test="boolean(./basedon)">
	<span><b>Na podstawie: </b></span><br />
	<xsl:apply-templates select="./basedon"/>
	<br />
	</xsl:if>
	</xsl:template>
	
	<xsl:template match="screenplay/basedon">
	<xsl:if test="./@work = 'various'">
	<span>dzieł różnych, autorstwa:</span><br />
	</xsl:if>
	<xsl:if test="./@work != 'various'">
	<span><i><xsl:value-of select="./@work"/></i> autorstwa:</span><br />
	</xsl:if>
	<xsl:apply-templates/>
	</xsl:template>
	
	
	<xsl:template match="film/cast">
	<xsl:apply-templates select="./actor"/>
	</xsl:template>
	
	<xsl:template match="cast/actor">
	<xsl:apply-templates/>
	<ul style="margin-top:0em; margin-bottom:0.5em" >
	<li>jako: <b><xsl:value-of select="./@character"/></b></li>
	</ul>
	</xsl:template>
	
	
	<xsl:template match="film/budget">
	<b>Budżet: </b>
	<xsl:value-of select="."/>
	<xsl:text> </xsl:text>
	<xsl:value-of select="./@unit"/>
	<br />
	</xsl:template>
	
	<xsl:template match="film/boxoffice">
	<b>Przychód: </b>
	<xsl:value-of select="."/>
	<xsl:text> </xsl:text>
	<xsl:value-of select="./@unit"/>
	<br />
	</xsl:template>
	
	<xsl:template match="film/profit">
	<b>Zysk: </b>
	<xsl:value-of select="."/>
	<xsl:text> </xsl:text>
	<xsl:value-of select="./@unit"/>
	<br />
	</xsl:template>
	
	
	
	<xsl:template match="film/reception">
	<xsl:apply-templates/>
	</xsl:template>
	
	<xsl:template match="reception/zeroten">
	<b><xsl:value-of select="./@source"/>: </b><xsl:value-of select="."/>
	<br />
	</xsl:template>
	
	<xsl:template match="reception/percent">
	<b><xsl:value-of select="./@source"/>: </b><xsl:value-of select="."/><xsl:text>%</xsl:text>
	<br />
	</xsl:template>
	
	
	
	<xsl:template match="acclaim">
	<h2>Najlepiej oceniane filmy</h2>
	<ul>
	<xsl:for-each select="./shortfilm">
	<li>
	<xsl:element name="a">
	<xsl:attribute name="href"><xsl:text>#</xsl:text><xsl:value-of select="concat(./@title, ./@year)"/></xsl:attribute>
	<xsl:value-of select="./@title"/>
	</xsl:element>
	<xsl:text>, </xsl:text>
	<xsl:value-of select="./@year"/>
	<xsl:text>, Rotten Tomatoes score: </xsl:text>
	<xsl:value-of select="./@rating"/>
	</li>
	</xsl:for-each>
	</ul>
	</xsl:template>
	
	<xsl:template match="cashy">
	<h2>Najbardziej kasowe filmy</h2>
	<ul>
	<xsl:for-each select="./shortfilm">
	<li>
	<xsl:element name="a">
	<xsl:attribute name="href"><xsl:text>#</xsl:text><xsl:value-of select="concat(./@title, ./@year)"/></xsl:attribute>
	<xsl:value-of select="./@title"/>
	</xsl:element>
	<xsl:text>, </xsl:text>
	<xsl:value-of select="./@year"/>
	<xsl:text>, Zysk: $</xsl:text>
	<xsl:value-of select="format-number(./@profit, '###,###,###.##')"/>
	</li>
	</xsl:for-each>
	</ul>
	</xsl:template>
	
</xsl:stylesheet>
