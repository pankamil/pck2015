<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
	
<fo:fop version="1.0">
    <referenced-fonts>
      <match font-family="Times"/>
    </referenced-fonts>
</fo:fop>

<xsl:template match="/">
<fo:root>
   <fo:layout-master-set>
     <fo:simple-page-master master-name="simple"
                   page-height="29.7cm" 
                   page-width="21cm"
                   margin-top="1cm" 
                   margin-bottom="2cm" 
                   margin-left="2cm" 
                   margin-right="2cm">
       <fo:region-body margin-top="1.5cm"/>
     </fo:simple-page-master>
   </fo:layout-master-set>
 
 
   <fo:page-sequence master-reference="simple">
   	<fo:flow flow-name="xsl-region-body">
   		<fo:block font-size="12pt" font-family="Times">
   	       <xsl:apply-templates select="base"/>
   	    </fo:block>
   	</fo:flow>
   </fo:page-sequence>
  </fo:root> 
 </xsl:template>
	
 <xsl:template match="base">
       	<xsl:apply-templates select = "films/film"/>
 </xsl:template>
	
	<xsl:template match="film">
		<fo:table table-layout="auto" width="100%" border="solid black 1px" break-after="page">
			<fo:table-column column-width="30mm"/>
			<fo:table-column column-width="140mm"/>

			<fo:table-body>
            	<xsl:apply-templates />
            </fo:table-body>
		</fo:table>
	</xsl:template>
	
	<xsl:template match="title">
		<fo:table-row>
        <fo:table-cell border="solid black 1px">
            <fo:block>
            <xsl:text>Title: </xsl:text>
			</fo:block>
        </fo:table-cell>
        <fo:table-cell border="solid black 1px">
			<fo:block>
				<xsl:for-each select="words">
				    <xsl:if test="@lang = 'en'">
						<xsl:value-of select="."/>
					</xsl:if>
			    </xsl:for-each>
	    	</fo:block>
		</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	
	<xsl:template match="date">
		<fo:table-row>
		<fo:table-cell border="solid black 1px">
        <fo:block>
        <xsl:text>Release date:</xsl:text>
        </fo:block>
		</fo:table-cell>
		<fo:table-cell border="solid black 1px">
        <fo:block>
		<xsl:value-of select="concat(@day,'.',@month,'.',@year)"/>
		</fo:block>
		</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	
	<xsl:template match="time">
		<fo:table-row>
		<fo:table-cell border="solid black 1px">
        <fo:block>
        <xsl:text>Length:</xsl:text>
        </fo:block>
		</fo:table-cell>
		<fo:table-cell border="solid black 1px">
        <fo:block>
		<xsl:value-of select="concat(. , ' ' , @unit)"/>
		</fo:block>
		</fo:table-cell>
		</fo:table-row>
		
	</xsl:template>
	
	<xsl:template match="genres">
		<fo:table-row>
		<fo:table-cell border="solid black 1px">
        <fo:block>
		<xsl:text>Genres: </xsl:text>
		</fo:block>
		</fo:table-cell>
		<fo:table-cell border="solid black 1px">
        <fo:block>
		<xsl:for-each select="genre">
		    <xsl:value-of select="."/>
		    <xsl:if test="position() != last()">
		    	<xsl:text>, </xsl:text>
		    </xsl:if>
	    </xsl:for-each>
	    </fo:block>
		</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	
	<xsl:template match="direction">
		<fo:table-row>
		<fo:table-cell border="solid black 1px">
        <fo:block>
		<xsl:text>Director: </xsl:text>
		</fo:block>
		</fo:table-cell>
		<fo:table-cell border="solid black 1px">
        <fo:block>
		    <xsl:apply-templates select="./director"/>
		</fo:block>
		</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	
	<xsl:template match="screenplay">
		<fo:table-row>
		<fo:table-cell border="solid black 1px">
        <fo:block>
		<xsl:text>Screenplay:</xsl:text>
		</fo:block>
		</fo:table-cell>
		<fo:table-cell border="solid black 1px">
        <fo:block>
		<xsl:apply-templates select="./writer"/>
		</fo:block>
		</fo:table-cell>
		</fo:table-row>
		
		<xsl:if test="story">
		<fo:table-row>
		<fo:table-cell border="solid black 1px">
        <fo:block>
	    	<xsl:text>Story:</xsl:text>
	    </fo:block>
		</fo:table-cell>
		<fo:table-cell border="solid black 1px">
        <fo:block>
		    <xsl:apply-templates select="./story"/>
		</fo:block>
		</fo:table-cell>
		</fo:table-row>
	    </xsl:if>
	    
	    <xsl:if test="basedon">
	    <fo:table-row>
		<fo:table-cell border="solid black 1px">
        <fo:block>
	    	<xsl:text>Based on </xsl:text>
	    </fo:block>
		</fo:table-cell>
		<fo:table-cell border="solid black 1px">
        <fo:block>
		    <xsl:apply-templates select="./basedon"/>
		</fo:block>
		</fo:table-cell>
		</fo:table-row>
	    </xsl:if>

		
	</xsl:template>
	
	<xsl:template match="basedon">
		<xsl:choose>
			<xsl:when test="@work = 'various'">
				<xsl:text>various works by:</xsl:text>
		    </xsl:when>
		    <xsl:otherwise>
		    	<fo:inline font-style="italic">
		    	<xsl:value-of select="concat('&quot;', @work, '&quot;')"/>
		    	</fo:inline>
		    	<xsl:text> by:</xsl:text>
		    </xsl:otherwise>
		</xsl:choose>
		<xsl:apply-templates/>
	</xsl:template>
	
	<xsl:template match="cast">
		<fo:table-row>
		<fo:table-cell border="solid black 1px">
	    <fo:block>
		<xsl:text>Cast: </xsl:text>
		</fo:block>
		</fo:table-cell>
		<fo:table-cell border="solid black 1px">
	    <fo:block>
		    <xsl:apply-templates select="./actor"/>
		</fo:block>
		</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	
	<xsl:template match="actor">
		<xsl:apply-templates/>
		<xsl:text>as: </xsl:text>
		<xsl:value-of select="@character"/>
	</xsl:template>
	
	<xsl:template match="budget">
		<fo:table-row>
		<fo:table-cell border="solid black 1px">
	    <fo:block>
			<xsl:text>Budget: </xsl:text>
		</fo:block>
		</fo:table-cell>
		<fo:table-cell border="solid black 1px">
	    <fo:block>
		    <xsl:value-of select="concat(. , ' ', @unit)"/>
		</fo:block>
		</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	
	<xsl:template match="boxoffice">
		<fo:table-row>
		<fo:table-cell border="solid black 1px">
	    <fo:block>
			<xsl:text>Box office: </xsl:text>
		</fo:block>
		</fo:table-cell>
		<fo:table-cell border="solid black 1px">
	    <fo:block>
		    <xsl:value-of select="concat(. , ' ', @unit)"/>
		</fo:block>
		</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	
	<xsl:template match="profit">
		<fo:table-row>
		<fo:table-cell border="solid black 1px">
	    <fo:block>
			<xsl:text>Profit: </xsl:text>
		</fo:block>
		</fo:table-cell>
		<fo:table-cell border="solid black 1px">
	    <fo:block>
		    <xsl:value-of select="concat(. , ' ', @unit)"/>
		</fo:block>
		</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	
	<xsl:template match="reception">
		<fo:table-row>
		<fo:table-cell border="solid black 1px">
	    <fo:block>
		<xsl:text>Reception:</xsl:text>
		</fo:block>
		</fo:table-cell>
		<fo:table-cell border="solid black 1px">
	    <fo:block>
		<xsl:apply-templates/>
		</fo:block>
		</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	
	<xsl:template match="zeroten">
		<fo:block>
		<xsl:value-of select="concat(. , ': ', @source)"/>
		</fo:block>
	</xsl:template>
	
	<xsl:template match="percent">
		<fo:block>
		<xsl:value-of select="concat(. , ': ', @source)"/>
		</fo:block>
	</xsl:template>
	
	<xsl:template match="description">
		<fo:table-row>
		<fo:table-cell border="solid black 1px">
	    <fo:block>
		<xsl:text>Description:</xsl:text>
		</fo:block>
		</fo:table-cell>
		<fo:table-cell border="solid black 1px">
	    <fo:block>
		<xsl:value-of select="."/>
		</fo:block>
		</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	
	<xsl:template match="person">
		<fo:block>
		<fo:inline font-weight="bold"><xsl:value-of select="name"/>
</fo:inline>
		</fo:block>
		<fo:block>
		<xsl:value-of select="concat('Birth: ',birth/date/@day,'.',birth/date/@month,'.',birth/date/@year, ' (', ageDuringPremiere, ' years old during premiere)')"/>
		</fo:block>
		<xsl:if test="death">
		<fo:block>
			<xsl:value-of select="concat('Death: ',death/date/@day,'.',death/date/@month,'.',death/date/@year)"/>
		</fo:block>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
