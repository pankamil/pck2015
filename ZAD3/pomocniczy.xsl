<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:date="http://exslt.org/dates-and-times" 
    extension-element-prefixes="date">
	<xsl:output method="xml" encoding="utf-8"/>
  	<xsl:template match="/base">
  			<xsl:text>&#xa;</xsl:text>
  			<xsl:element name="base">
  			<xsl:text>&#xA;</xsl:text>
			<xsl:apply-templates select="films|header"/>
			<xsl:text>&#xA;</xsl:text>
			<xsl:element name="generationDate"><xsl:value-of select="date:date-time()"/></xsl:element>
			<xsl:text>&#xA;</xsl:text>
			</xsl:element>
	</xsl:template>
	<xsl:template match="base/header">
		<xsl:element name="header">
		<xsl:for-each select="student">
		    <student>
		    	<xsl:value-of select="name"/>
		    </student>
	    </xsl:for-each>
	    </xsl:element>
	</xsl:template>
	<xsl:template match="base/films">
		<xsl:text>&#xa;</xsl:text>
		<films>
		<xsl:text>&#xa;</xsl:text>
		<count>
			<xsl:value-of select="count(./film)"/>
		</count>
		<xsl:apply-templates>
		<xsl:sort select="title/words[@lang='en']" data-type="text" lang="en" />
		</xsl:apply-templates>
		<xsl:text>&#xa;</xsl:text>
		</films>
		<xsl:text>&#xa;</xsl:text>
		<xsl:element name="acclaim">
			<xsl:for-each select="./film">
				<xsl:sort select="./reception/percent" order="descending" data-type="text" lang="en" />
				<xsl:text>&#xa;</xsl:text>

				<xsl:element name="shortfilm">
					<xsl:attribute name="title">
						<xsl:value-of select="./title/words[@lang='en']"/>
					</xsl:attribute >
					<xsl:attribute  name="year">
						<xsl:value-of select="./date/@year"/>
					</xsl:attribute >
					<xsl:attribute  name="rating">
						<xsl:value-of select="./reception/percent"/>
					</xsl:attribute >
				</xsl:element>
			</xsl:for-each>
			<xsl:text>&#xa;</xsl:text>
		</xsl:element>
		<xsl:text>&#xa;</xsl:text>
		<xsl:element name="cashy">
			<xsl:for-each select="./film">
				<xsl:sort select="(./boxoffice - ./budget)" order="descending" data-type="number" lang="en" />
				<xsl:text>&#xa;</xsl:text>

				<xsl:element name="shortfilm">
					<xsl:attribute name="title">
						<xsl:value-of select="./title/words[@lang='en']"/>
					</xsl:attribute >
					<xsl:attribute  name="year">
						<xsl:value-of select="./date/@year"/>
					</xsl:attribute >
					<xsl:attribute  name="profit">
						<xsl:value-of select="format-number((./boxoffice - ./budget) * 1000000, '#.00')"/>
					</xsl:attribute >
				</xsl:element>
			</xsl:for-each>
			<xsl:text>&#xa;</xsl:text>
		</xsl:element>
	</xsl:template>
	<xsl:template match="film">
		<film>
		<xsl:copy-of select="./title"/>
		<xsl:copy-of select="./date"/>
		<age><xsl:value-of select="2015 - ./date/@year"/></age>
		<xsl:copy-of select="./time"/>
		<xsl:copy-of select="./genres"/>
		<xsl:apply-templates select="direction"/>
		<xsl:apply-templates select="screenplay"/>
		<xsl:apply-templates select="cast"/>
		<xsl:text>&#xa;</xsl:text>
		<xsl:copy-of select="./budget"/>
		<xsl:copy-of select="./boxoffice"/>
		<profit>
			<xsl:attribute name="unit">
			    <xsl:value-of select="./budget/@unit" />
			</xsl:attribute>
			<xsl:value-of select="format-number(./boxoffice - ./budget, '#.0')"/>
		</profit>
		<xsl:copy-of select="./reception"/>
		<xsl:copy-of select="./description"/>
		</film>
	</xsl:template>
	<xsl:template match="direction">
		<xsl:text>&#xa;</xsl:text>
		<xsl:variable name="year" select="../date/@year"/>
		<direction>
			<xsl:for-each select="./director">
				<xsl:variable name="dirid" select="@id"/>
				
				<xsl:text>&#xa;</xsl:text>
				<director>
					<person>
					<xsl:copy-of select="//person[@id = $dirid]/name"/>
					<xsl:copy-of select="//person[@id = $dirid]/birth"/>
					<xsl:copy-of select="//person[@id = $dirid]/death"/>
					<ageDuringPremiere><xsl:value-of select="$year - //person[@id = $dirid]/birth//date/@year"/></ageDuringPremiere>
					</person>
				</director>
			</xsl:for-each>
		</direction>
	</xsl:template>
	
	<xsl:template match="screenplay">
		<xsl:text>&#xa;</xsl:text>
		<xsl:variable name="year" select="../date/@year"/>
		<screenplay>
			<xsl:for-each select="./*">
				<xsl:variable name="name" select="local-name()"/>
				<xsl:variable name="dirid" select="@id"/>
				<xsl:text>&#xa;</xsl:text>
				<xsl:element name = "{$name}">
					<xsl:if test="$name = 'basedon'">
						<xsl:attribute name="work"><xsl:value-of select="@work"/></xsl:attribute>
					</xsl:if>
					<person>
					<xsl:copy-of select="//person[@id = $dirid]/name"/>
					<xsl:copy-of select="//person[@id = $dirid]/birth"/>
					<xsl:copy-of select="//person[@id = $dirid]/death"/>
					<ageDuringPremiere><xsl:value-of select="$year - //person[@id = $dirid]/birth//date/@year"/></ageDuringPremiere>
					</person>
				</xsl:element>
			</xsl:for-each>
		</screenplay>
	</xsl:template>
	<xsl:template match="cast">
		<xsl:text>&#xa;</xsl:text>
		<xsl:variable name="year" select="../date/@year"/>
		<cast>
			<xsl:for-each select="./actor">
				<xsl:variable name="dirid" select="@id"/>
				<xsl:text>&#xa;</xsl:text>
				<actor>
					<xsl:attribute name="character"><xsl:value-of select="@character"/></xsl:attribute>
					<person>
					<xsl:copy-of select="//person[@id = $dirid]/name"/>
					<xsl:copy-of select="//person[@id = $dirid]/birth"/>
					<xsl:copy-of select="//person[@id = $dirid]/death"/>
					<ageDuringPremiere><xsl:value-of select="$year - //person[@id = $dirid]/birth//date/@year"/></ageDuringPremiere>
					</person>
				</actor>
			</xsl:for-each>
		</cast>
	</xsl:template>
</xsl:stylesheet>
