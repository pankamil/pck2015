<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg">
	
<xsl:output method="xml" indent="yes" standalone="no" doctype-public="-//W3C//DTD SVG 1.1//EN" doctype-system="http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd" media-type="image/svg" />

	<xsl:variable name="svg_width" select="400"/>
	<xsl:variable name="svg_height" select="300"/>
	<xsl:variable name="padding" select="5"/>
	
	<!-- wykres pokazuje wiek każdego filmu -->
	
	
	<xsl:variable name="x_width" select="($svg_width - 2*$padding) div //base/films/count"/>
	<xsl:variable name="y_steps">
	    <xsl:for-each select="//base/films/film">
		    <xsl:sort select="age" data-type="number" order="descending"/>
		    <xsl:if test="position() = 1">
				<xsl:value-of select="($svg_height - 2*$padding - 20) div age"/>
		    </xsl:if>
	    </xsl:for-each>
	</xsl:variable>
	
	<!-- tło -->
	<xsl:template match="/">
		<svg width="{$svg_width + 200}" height="{$svg_height + 200}"  onload="init(evt)">
	      y_steps = <xsl:value-of select="$y_steps"/>
		<rect id="back" x="1" y="1" width="{$svg_width - 2}" height="{$svg_height - 2}" fill="yellow" stroke="blue">
		
		<animate 
	    attributeName="fill"
	    values="yellow;blue;green;yellow" 
  		keyTimes="0;0.33;0.66;1" 
	    dur="10s"
	    begin="0s"
	    repeatCount="indefinite"/>
	    </rect>
		<xsl:apply-templates select="base"/>
		
		<style>
		    .caption{
			font-size: 14px;
			font-family: Georgia, serif;
		    }
		    .tooltip{
			font-size: 12px;
		    }
		    .tooltip_bg{
			fill: white;
			stroke: black;
			stroke-width: 1;
			opacity: 0.85;
		    }
		    .label{
			font-size: 20px;
			font-family: Impact;
			fill: white;
			stroke: black;
		    }
		</style>
	
		<script>
		    <![CDATA[
		
			function init(evt)
			{
			    if ( window.svgDocument == null )
			    {
				svgDocument = evt.target.ownerDocument;
			    }
		
			    tooltip = svgDocument.getElementById('tooltip');
			    tooltip_bg = svgDocument.getElementById('tooltip_bg');
		
			}
		
			function ShowTooltip(evt, mouseovertext)
			{
			    tooltip.setAttributeNS(null,"x",evt.clientX+11);
			    tooltip.setAttributeNS(null,"y",evt.clientY+27);
			    tooltip.firstChild.data = mouseovertext;
			    tooltip.setAttributeNS(null,"visibility","visible");
		
			    length = tooltip.getComputedTextLength();
			    tooltip_bg.setAttributeNS(null,"width",length+8);
			    tooltip_bg.setAttributeNS(null,"x",evt.clientX+8);
			    tooltip_bg.setAttributeNS(null,"y",evt.clientY+14);
			    tooltip_bg.setAttributeNS(null,"visibility","visibile");
			}
		
			function HideTooltip(evt)
			{
			    tooltip.setAttributeNS(null,"visibility","hidden");
			    tooltip_bg.setAttributeNS(null,"visibility","hidden");
			}
		
		    ]]>
		</script>
		
		
		</svg>
	</xsl:template>
	
	<xsl:template match="base">
		<xsl:apply-templates select="films/film"/>
		<rect class="tooltip_bg" id="tooltip_bg"
 x="0" y="0" rx="4" ry="4"
 width="52" height="16" visibility="hidden"/>
		<text class="tooltip" id="tooltip" x="0" y="0" visibility="hidden">Tooltip</text>
		<text class="caption" x="20" y="330">Years since premiere</text>
	</xsl:template>
	
	<xsl:template match="film">
	   
    <xsl:variable name="tip">
		<xsl:text>&apos;</xsl:text> 
		<xsl:value-of select="./title/words[1]"/>
		<xsl:text>&apos;</xsl:text> 
    </xsl:variable> 

	<!-- odnośnik do html po kliknięciu na słupek -->

	<a xlink:href="{concat('result.html', '#', ./title/words[@lang='en'], ./date/@year)}">
	
		<!-- słupki -->
	    <rect x="{$padding + $x_width * (position() - 1) }" width="{$x_width}" fill="red" stroke="black" onmousemove="ShowTooltip(evt, {$tip})" onmouseout="HideTooltip()">
	    <animate 
		    attributeName="height"
		    dur="3s"
		    fill="freeze"
		    from="0" to="{age * $y_steps + 1}" />
		<animate 
		    attributeName="y"
		    dur="3s"
		    fill="freeze"
		    from="{$svg_height - $padding}" to="{$svg_height - $padding - (age * $y_steps + 1)}" />
		</rect>
		
		<!-- liczba nad słupkiem -->
		
<text class="label" x="{$padding + $x_width * (position() - 1) + 2 + number(age &lt; 10)*5}" onmousemove="ShowTooltip(evt, {$tip})" onmouseout="HideTooltip()">
		<xsl:value-of select="age"/>
		
<animate 
		    attributeName="y"
		    dur="3s"
		    fill="freeze"
		    from="{$svg_height - $padding}" to="{$svg_height - $padding - (age * $y_steps + 1)}" />
		
</text>
    
</a>

 	</xsl:template>

</xsl:stylesheet>
